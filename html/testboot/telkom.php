<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Data Mahasiswa</title>
</head>

<body>
<h1 class="page-header">
                    Data Mahasiswa Jurusan Teknik Elektro
                    </h1>
<div class="row">
<div class="col-lg-3">
<div class="panel panel-default">

                            <div class="panel-heading">
                                <h3 class="panel-title">D3-Teknik Elektro</h3>
                            </div>
                            <div class="panel-body">
                                <p><a href="?menu=datamah&prodi=D3-Teknik Elektro&kelas=1A">Kelas 1A</a>                                  </p>
                                <p><a href="?menu=datamah&prodi=D3-Teknik Elektro&kelas=1B">Kelas 1B</a>                                  </p>
                                <p><a href="?menu=datamah&prodi=D3-Teknik Elektro&kelas=2A">Kelas 2A</a>                                  </p>
                                <p><a href="?menu=datamah&prodi=D3-Teknik Elektro&kelas=2B">Kelas 2B</a>                                  </p>
                                <p><a href="?menu=datamah&prodi=D3-Teknik Elektro&kelas=3A">Kelas 3A</a>                                  </p>
                                <p><a href="?menu=datamah&prodi=D3-Teknik Elektro&kelas=3B">Kelas 3B</a></p>
                                
                            </div>
                        </div>

<div class="panel panel-default">

                            <div class="panel-heading">
                                <h3 class="panel-title">D4-Teknik Elektro</h3>
                            </div>
                            <div class="panel-body">
                                <p><a href="?menu=datamah&prodi=D4-Teknik Elektro&kelas=1EK">Kelas 1EK</a>                                  </p>
                                <p><a href="?menu=datamah&prodi=D4-Teknik Elektro&kelas=2EK">Kelas 2EK</a>                                  </p>
                                <p><a href="?menu=datamah&prodi=D4-Teknik Elektro&kelas=3EK">Kelas 3EK</a>                                  </p>
								<p><a href="?menu=datamah&prodi=D4-Teknik Elektro&kelas=4EK">Kelas 4EK</a>                                  </p>
                                
                                
                            </div>
                        </div>
</div>
<div class="row">
<div class="col-lg-3">
<div class="panel panel-info">

                            <div class="panel-heading">
                                <h3 class="panel-title">D3-Teknik Telekomunikasi</h3>
                            </div>
                            <div class="panel-body">
                                <p><a href="?menu=datamah&prodi=D3-Teknik Telekomunikasi&kelas=1A">Kelas 1A</a>                                  </p>
                                <p><a href="?menu=datamah&prodi=D3-Teknik Telekomunikasi&kelas=1B">Kelas 1B</a>                                  </p>
                                <p><a href="?menu=datamah&prodi=D3-Teknik Telekomunikasi&kelas=2A">Kelas 2A</a>                                  </p>
                                <p><a href="?menu=datamah&prodi=D3-Teknik Telekomunikasi&kelas=2B">Kelas 2B</a>                                  </p>
                                <p><a href="?menu=datamah&prodi=D3-Teknik Telekomunikasi&kelas=3A">Kelas 3A</a>                                  </p>
                                <p><a href="?menu=datamah&prodi=D3-Teknik Telekomunikasi&kelas=3B">Kelas 3B</a>                                  </p>
                            </div>
                        </div>
                        <div class="panel panel-info">

                            <div class="panel-heading">
                                <h3 class="panel-title">D4-Teknik Telekomunikasi</h3>
                            </div>
                            <div class="panel-body">
                                <p><a href="?menu=datamah&prodi=D4-Teknik Telekomunikasi&kelas=1NK">Kelas 1NK</a>                                  </p>
                                <p><a href="?menu=datamah&prodi=D4-Teknik Telekomunikasi&kelas=2NK">Kelas 2NK</a>                                  </p>
                                <p><a href="?menu=datamah&prodi=D4-Teknik Telekomunikasi&kelas=3NK">Kelas 3NK</a>                                  </p>
                                <p><a href="?menu=datamah&prodi=D4-Teknik Telekomunikasi&kelas=4NK">Kelas 4NK</a>
                            </div>
                        </div>


</div>
<div class="row">
<div class="col-lg-3">
<div class="panel panel-red">

                            <div class="panel-heading">
                                <h3 class="panel-title">D3-Teknik Listrik</h3>
                            </div>
                            <div class="panel-body">
                                <p><a href="?menu=datamah&prodi=D3-Teknik Listrik&kelas=1A">Kelas 1A</a>                                  </p>
                                <p><a href="?menu=datamah&prodi=D3-Teknik Listrik&kelas=1B">Kelas 1B</a>                                  </p>
                                <p><a href="?menu=datamah&prodi=D3-Teknik Listrik&kelas=2A">Kelas 2A</a>                                  </p>
                                <p><a href="?menu=datamah&prodi=D3-Teknik Listrik&kelas=2B">Kelas 2B</a>                                  </p>
                                <p><a href="?menu=datamah&prodi=D3-Teknik Listrik&kelas=3A">Kelas 3A</a>                                  </p>
                                <p><a href="?menu=datamah&prodi=D3-Teknik Listrik&kelas=3B">Kelas 3B</a>                                  </p>
                            </div>
                        </div>
                        <div class="panel panel-red">

                            <div class="panel-heading">
                                <h3 class="panel-title">D4-Teknik Otomasi Industri</h3>
                            </div>
                            <div class="panel-body">
                                <p><a href="?menu=datamah&prodi=D4-Teknik Otomasi Industri&kelas=1OI">Kelas 1OI</a>                                  </p>
                                <p><a href="?menu=datamah&prodi=D4-Teknik Otomasi Industri&kelas=2OI">Kelas 2OI</a>                                  </p>
                                <p><a href="?menu=datamah&prodi=D4-Teknik Otomasi Industri&kelas=3OI">Kelas 3OI</a>                                  </p>
								<p><a href="?menu=datamah&prodi=D4-D4-Teknik Otomasi Industri&kelas=4OI">Kelas 4OI</a>                                  </p>
                                
                            </div>
                        </div>


</div>

                   
                    

</body>
</html>