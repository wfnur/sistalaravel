@extends('Layout.master')

@section('title','Mahasiswa')

@section('navbar')
<!-- Navbar -->
<nav class="main-header navbar navbar-expand bg-primary navbar-dark ">
  <!-- Left navbar links -->
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
    </li>
    <li class="nav-item d-none d-sm-inline-block">
        <a href="{{url('/Mahasiswa/Beranda')}}" class="nav-link">Beranda</a>
    </li>
    <li class="nav-item d-none d-sm-inline-block">
        <a href="{{url('/Mahasiswa/Profile')}}" class="nav-link">Ubah Profile</a>
    </li>
  </ul>  
</nav>
<!-- /.navbar -->
@stop

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1 class="m-0 text-dark">Beranda</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item active">Beranda</li>
        </ol>
        </div><!-- /.col -->
    </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
    
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <section class="col-12">
        <!-- Custom tabs (Charts with tabs)-->
        <div class="card">
            <div class="card-body" style="padding:30px">
                <div class="row">
                    <div class="col-12">
                        <center><h1>Selamat Datang {{ $user->nama }} </h1>
                        <p class='btn btn-primary'>Data Bimbingan dan lainnya akan segera
                        menyusul dikarenakan website menggunakan sistem yang baru</p>
                        </center>
                    </div>
                </div>
            </div><!-- /.card-body -->
        </div>
        <!-- /.card -->

        </section>
        <!-- /.Left col -->
        
    </div>
    <!-- /.row (main row) -->

    <div class="row">
            <div class="col-6">
                    <div class="card card-default">
                        <div class="card-header bg-warning">
                            <h3 class="card-title">
                                <i class="fa fa-bullhorn"></i>
                                Revisi Proposal TA
                            </h3>
                        </div>
        
                        <div class="card-body">
                            <p>{!! $reviewPTA->revisi or '' !!}</p>
                        </div>
                    
                    
                    </div>
                </div>
        <div class="col-6">
            <div class="card card-default">
                <div class="card-header bg-warning">
                    <h3 class="card-title">
                        <i class="fa fa-bullhorn"></i>
                        Revisi Laporan
                    </h3>
                </div>

                <div class="card-body">
                @foreach ($revisi as $item)
                    @php 
                    $statusRev = cekRevisiLaporan(auth()->user()->username,$item->kode_dosen); 
                    //dd($item->kode_dosen);
                    
                    @endphp
                    <div class="callout callout-warning">
                        <h5>{{$item->dosen->nama}} 
                        @if ($statusRev == 1)
                        <span class="btn btn-success"> Tidak Ada Revisi</span>
                        @else
                        <span class="btn btn-danger"> Masih Harus Revisi</span>
                        @endif  
                        </h5>
                        <p>{!! $item->revisi !!}</p>
                    </div>
                @endforeach
                </div>
            
            
            </div>
        </div>
    </div>
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->   
@endsection